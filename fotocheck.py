#!/usr/bin/python3.10

"""
Fotocheck - simple checker of photo parameters
"""

__version__ = "2022"
__author__ = "Jaroslav Svoboda"

from prettytable import PrettyTable
from libxmp.utils import file_to_dict
from libxmp import consts

# import pandas as pd

from PIL import Image
from PIL.ExifTags import TAGS
from datetime import datetime
import os
import argparse
import magic

def_foto_dir = "foto"

sem_2_first_day = 14
longer_side = 1500
ppi = 72
jpeg_mime = "image/jpeg"
email_string = "Iptc4xmpCore:CreatorContactInfo/Iptc4xmpCore:CiEmailWork"
true_string = "OK"
false_string = "\u00D7"
none_string = "NONE"


""" get values """


def get_current_semester_start():
    now = datetime.now()
    month = int(now.strftime("%m"))
    week = int(now.strftime("%V"))
    if month < 9 and week > 7:
        sem_start_date = datetime(
            int(now.strftime("%Y")), 2, sem_2_first_day, 0, 0
        )
    elif month > 8:
        sem_start_date = datetime(int(now.strftime("%Y")), 9, 1, 0, 0)
    elif week < 7:
        sem_start_date = datetime(int(now.strftime("%Y") - 1), 9, 1, 0, 0)
    else:
        print("I am drunk and I have no idea what day is today!")
    return sem_start_date


def get_exif_item(exiv, field):
    # print("--------")
    for (k, v) in exiv.items():
        # print(f'{k}:{v}')
        if TAGS.get(k) == field:
            return v


def get_date_diff(exiv, sem_start_date):
    # tag 36867
    if get_exif_item(exiv, "DateTimeOriginal") is None:
        return None
    else:
        try:
            n = (
                datetime.strptime(
                    get_exif_item(exiv, "DateTimeOriginal").rstrip("\x00"),
                    "%Y:%m:%d %H:%M:%S",
                )
                - sem_start_date
            )
            return n
        except ValueError:
            return None


def get_ppi(exif):
    ppi_x = get_exif_item(exif, "XResolution")
    ppi_y = get_exif_item(exif, "YResolution")
    ppi_unit = get_exif_item(exif, "ResolutionUnit")
    return ppi_x, ppi_y, ppi_unit


def get_iptc_core(xmp):
    try:
        iptc_core = xmp[consts.XMP_NS_IPTCCore]
        return iptc_core
    except KeyError:
        return None


def get_filetype(filepath):
    mime = magic.Magic(mime=True)
    try:
        mimetype = mime.from_file(os.path.join(filepath))
    except IsADirectoryError:
        mimetype = "folder"
    return mimetype
    # return mimetypes.guess_type(file)[0]


def get_artist(exif):
    if get_exif_item(exif, "Artist") is None:
        return None
    else:
        return get_exif_item(exif, "Artist").encode("8859").decode("UTF-8")


def get_res(image):
    return max(image.size)


def get_copyright(exif):
    if get_exif_item(exif, "Copyright") is None:
        return None
    else:
        cprght = get_exif_item(exif, "Copyright").encode("8859").decode("UTF-8")
        return cprght


def get_email(iptc_core):
    for i in iptc_core:
        if email_string in i:
            email = i[1]
            break
        else:
            email = None
    return email


def get_jpeg_data(afile, imagefile):
    result = {}
    sem_start_date = get_current_semester_start()
    # print(afile)
    result["filename"] = afile
    result["mimetype"] = get_filetype(imagefile)
    image = Image.open(imagefile)
    result["resolution"] = get_res(image)
    exif = image.getexif()
    exiv = image._getexif()
    # print(exiv)
    xmp = file_to_dict(imagefile)
    iptc_core = get_iptc_core(xmp)
    if exif is not None:
        result["date"] = get_date_diff(exiv, sem_start_date)
        ppi_x, ppi_y, ppi_unit = get_ppi(exif)
        result["ppi_x"] = ppi_x
        result["ppi_y"] = ppi_y
        result["ppi_unit"] = ppi_unit
        result["artist"] = get_artist(exif)
        result["copyright"] = get_copyright(exif)
    if iptc_core is not None:
        result["email"] = get_email(iptc_core)
    else:
        result["email"] = None
    return result


def get_data(foto_dir=def_foto_dir):
    results = []

    for afile in os.listdir(foto_dir):
        filepath = os.path.join(foto_dir, afile)
        if get_filetype(filepath) == jpeg_mime:
            imagefile = filepath
            result = get_jpeg_data(afile, imagefile)
            results.append(result)
            # print(image.getexif())
        else:
            result = {"filename": afile, "mimetype": get_filetype(filepath)}
            results.append(result)

    return results


""" evaluate values """


def eval_filetype(result):
    if result["mimetype"] == jpeg_mime:
        return True
    else:
        return False


def eval_ppi(result):
    if (
        result.get("ppi_x") == ppi
        and result.get("ppi_y") == ppi
        and result.get("ppi_unit") == 2
    ):
        return True
    else:
        return False


def eval_artist(result):
    if result.get("artist") is None:
        return None
    elif result.get("artist") == "":
        return False
    else:
        return True


def eval_copyright(result):
    if result.get("copyright") is None:
        return None
    elif result.get("copyright") == "":
        return False
    else:
        return True


def eval_author(result):
    if eval_artist(result) is None or eval_artist(result) is False:
        if eval_copyright(result) is None or eval_copyright(result) is False:
            return False
        else:
            return True
    else:
        return True


def eval_res(result):
    if result.get("resolution") is None:
        return None
    elif result.get("resolution") == longer_side:
        return True
    elif result.get("resolution") > longer_side:
        return False
    elif result.get("resolution") < longer_side:
        return False
    else:
        return None


def eval_date(result):
    if result.get("date") is None:
        return None
    elif str(result.get("date"))[0] == "-":
        return False
    else:
        return True


def eval_email(result):
    if (
        result.get("email") and result.get("artist") and result.get("copyright")
    ) is None:
        return None
    elif "@" and "." in (
        result.get("email") or result.get("artist") or result.get("artist")
    ):
        return True
    else:
        return False


def evaluate_result(results):
    eval_results = []
    for result in results:
        eval_result = {}
        eval_result["filename"] = result.get("filename")
        eval_result["jpeg"] = eval_filetype(result)
        eval_result["date"] = eval_date(result)
        eval_result["longer_side"] = eval_res(result)
        eval_result["ppi"] = eval_ppi(result)
        eval_result["author"] = eval_author(result)
        eval_result["email"] = eval_email(result)
        eval_results.append(eval_result)

    return eval_results


def use_symbols(results):
    print_results = []
    for result in results:
        if result is True:
            print_result = true_string
        elif result is False:
            print_result = false_string
        elif result is None:
            print_result = none_string
        else:
            print_result = result
        print_results.append(print_result)

    return print_results


""" Display data """


def show_all(results):
    all_res = PrettyTable(
        [
            "FILENAME",
            "FILETYPE",
            "RESOLUTION",
            "DATE",
            "PPI X",
            "PPI Y",
            "PPI UNIT",
            "ARTIST",
            "COPYRIGHT",
            "IPTC-EMAIL",
        ]
    )
    for result in results:
        row = list(result.values())
        while len(row) < 10:
            row.append(None)
        all_res.add_row(row)
    print(all_res)


def show_errors(eval_results):
    err = PrettyTable()
    err.field_names = [
        "FILENAME",
        "JPEG",
        "DATE",
        str(longer_side) + "px",
        "PPI=" + str(ppi),
        "AUTHOR",
        "EMAIL",
    ]
    # err_results = []
    for eval_result in eval_results:
        if False in list(eval_result.values()):
            # err_results.append(eval_result)
            row = use_symbols(list(eval_result.values()))
            while len(row) < 6:
                row.append(None)
            err.add_row(row)

    # dfe = pd.DataFrame(err_results)
    # dfe_no_indices = dfe.to_string(index=False)
    # print(dfe_no_indices)
    print(err)


def start(foto_dir=def_foto_dir):
    parser = argparse.ArgumentParser(
        description="Fotocheck, simple homework checker for KME",
        epilog="That's all folks!",
    )
    parser.add_argument(
        "-i", "--input", type=str, help="Path to directory with homeworks"
    )
    parser.add_argument(
        "-t", "--type", type=str, help='Either "all" or "errors"'
    )

    args = parser.parse_args()

    results = get_data()

    if args.type == "all":
        show_all(results)

    elif args.type == "error":
        eval_results = evaluate_result(results)
        show_errors(eval_results)
    else:
        parser.print_help()


start()
