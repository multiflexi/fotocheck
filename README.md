fotocheck - a simple photo checking script
Requires Python 3.10 and exempi
macOS Homebrew:
```
brew install python@3.10 exempi
brew link --force python@3.10 
```
Clone repo and install modules:
```
git clone https://gitlab.com/multiflexi/fotocheck.git
cd fotocheck
pip3 install -r requirements.txt
```
